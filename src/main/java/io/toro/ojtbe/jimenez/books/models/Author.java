package io.toro.ojtbe.jimenez.books.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
public final class Author {
    private @Id @GeneratedValue Long id;
    private String name;

    @ManyToMany
    @JoinTable(name = "book_author", joinColumns = { @JoinColumn(name = "fk_author")},
    inverseJoinColumns = { @JoinColumn(name = "fk_book")})
    private List<Book> books = new ArrayList<>();

    public Author(String name){
        this.name = name;
    }

    public void addBook(Book newBook){
        if(!books.contains(newBook)){
            books.add(newBook);
            newBook.addAuthor(this);
        }
    }
}
