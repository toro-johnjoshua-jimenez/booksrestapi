package io.toro.ojtbe.jimenez.books.database.seeder;

import io.toro.ojtbe.jimenez.books.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public final class DefaultSeeder implements CommandLineRunner {
    private final BookRepository bookRepo;
    private final CategoryRepository categoryRepo;
    private final AuthorRepository authorRepo;

    @Autowired
    public DefaultSeeder(BookRepository bookRepo, CategoryRepository categoryRepo, AuthorRepository authorRepo){
        this.bookRepo = bookRepo;
        this.categoryRepo = categoryRepo;
        this.authorRepo = authorRepo;
    }

    @Override
    public void run(String... strings) throws Exception{
        Author a = new Author("Joshua");
        Author b = new Author("Robert");
        Author c = new Author("Kiddo");

        Category x = new Category("Entertainment");
        Category y = new Category("Horror");
        Category z = new Category("Drama");


        Book g = new Book("HP1",x);
        Book h = new Book("HP2",y);
        Book i = new Book("HP3",z);
        Book j = new Book("HP4",x);
        Book k = new Book("HP5",y);
        Book l = new Book("HP6",z);

        categoryRepo.save(x);
        categoryRepo.save(y);
        categoryRepo.save(z);

        bookRepo.save(g);
        bookRepo.save(h);
        bookRepo.save(i);
        bookRepo.save(j);
        bookRepo.save(k);
        bookRepo.save(l);

        g.addAuthor(a);
        g.addAuthor(b);

        h.addAuthor(c);
        h.addAuthor(a);

        i.addAuthor(b);
        i.addAuthor(c);

        j.addAuthor(a);
        k.addAuthor(b);
        l.addAuthor(c);

        authorRepo.save(a);
        authorRepo.save(b);
        authorRepo.save(c);
    }
}
