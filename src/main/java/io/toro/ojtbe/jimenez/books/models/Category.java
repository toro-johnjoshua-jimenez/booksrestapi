package io.toro.ojtbe.jimenez.books.models;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public final class Category {
    private @Id @GeneratedValue Long id;
    private String name;

    private Category() {}

    public Category(String name){
        this.name = name;
    }
}
