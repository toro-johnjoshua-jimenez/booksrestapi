package io.toro.ojtbe.jimenez.books.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
public final class Book {
    private @Id @GeneratedValue Long id;
    private String bookName;

    @OneToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToMany(mappedBy = "books")
    private List<Author> authors = new ArrayList<>();

    public Book(String bookName, Category category){
        this.bookName = bookName;
        this.category = category;
    }

    public void addAuthor(Author newAuthor){
        if(!authors.contains(newAuthor)){
            authors.add(newAuthor);
            newAuthor.addBook(this);
        }
    }
}
